package co.id.truemoney.Dto;

import java.sql.Date;
import lombok.Data;

import java.sql.Time;
import java.sql.Timestamp;

@Data
public class TmnPrepaidDto {
    private Integer id;
    private String idtransaksi;
    private String idtransaksiSupplier;
    private Date tanggal;
    private Time waktu;
    private String idAgent;
    private String idAgentAccount;
    private String idAgentCard;
    private String namaAgent;
    private String lokasi;
    private String idMember;
    private String stockType;
    private String channel;
    private String subTipeTransaksi;
    private String sn;
    private String statusTraksaksi;
    private String keterangan;
    private String tipeTransaksi;
    private Double nominal;
    private String namaSupplier;
    private String namaOperator;
    private Double hargaBeli;
    private Double hargaJual;
    private Double hargaCetakMemberOrAgent;
    private Double revenueFromTrueToMemberOrAgent;
    private Double revenueFromSupplierToTrue;
    private Double grossRevenue;
    private Double komisiAgen;
    private Double komisiDealer;
    private Double netRevenue;
    private Double cashBack;
    private Double netProvit;
    private Timestamp timestampupdate;
    private Timestamp timestamprefund;
    private Integer flagClean;
    private Integer flagFinnacne;
    private String noHpTujuan;
    private Double hargaCetakFp;
    private Double potonganHargaFp;
    private Double hargaCetakKurangHargaBeliFp;
    private Double grossRevenueFp;
    private String tipeKomisi;
    private Double total;
    private Double hargaCetakPromo;
    private Double promo;
    private String promoType;
    private Double bonus;
    private String idPelanggan;
    private String namaApplicant;
    private String applicantNationalId;
    private String transactionIdGateway;
    private Timestamp importeddate;
}
