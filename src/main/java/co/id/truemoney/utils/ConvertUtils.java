/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.id.truemoney.utils;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 * @author dhimas
 */
public class ConvertUtils {
    
    public static Date converDate(String Date) throws ParseException{
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date = sdf.parse(Date);
        java.sql.Date sqlStartDate = new java.sql.Date(date.getTime()); 
        return sqlStartDate;
    }
    
    public static Time converTime(String Date) throws ParseException{
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        java.util.Date date = sdf.parse(Date);
        java.sql.Time sqlStartDate = new java.sql.Time(date.getTime()); 
        return sqlStartDate;
    }
    
    public static Timestamp converTimeStamp(String Date) throws ParseException{
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        java.util.Date date = sdf.parse(Date);
        java.sql.Timestamp sqlStartDate = new java.sql.Timestamp(date.getTime()); 
        return sqlStartDate;
    }
    
    public static Double DoubleCheckIfNull(Double x){
        Double n =0.0;
        if(NullChecker.notNull(x)){
            n =x;
        }
        return n;
    }
    public static String StringCheckIfNull(String x){
        String n ="-";
        if(x==null){
            x =n;
        }
        return x;
    }



}
