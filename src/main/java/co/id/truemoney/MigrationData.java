/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.id.truemoney;

import co.id.truemoney.Dto.TmnPrepaidDto;
import co.id.truemoney.service.FetchDataService;
import co.id.truemoney.service.InsertDataService;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author dhimas
 */
public class MigrationData {
    
    final static Logger LOG = Logger.getLogger(MigrationData.class);

    public static void migrationData() throws IOException {
        Properties prop = new Properties();
        prop.load(MainApp.class.getClassLoader().getResourceAsStream("config.properties"));
        Integer decrementOfDay = Integer.valueOf(prop.getProperty("decrementOfDay"));

        ExecutorService executor = java.util.concurrent.Executors.newFixedThreadPool(10);

        final FetchDataService fetchDataService = new FetchDataService();
        final InsertDataService insertDataService = new InsertDataService();

        Calendar start = Calendar.getInstance();
//        start.set(Calendar.DATE, 24);
        start.set(Calendar.HOUR_OF_DAY, 00);
        start.set(Calendar.MINUTE, 0);
        start.set(Calendar.SECOND, 1);
        start.add(Calendar.DATE, decrementOfDay);
        Timestamp setStartDate1 = new Timestamp(start.getTime().getTime());
        Date deleteStart = new Date(setStartDate1.getTime());

        Calendar end = Calendar.getInstance();
//        end.set(Calendar.DATE, 24);
        end.set(Calendar.HOUR_OF_DAY, 23);
        end.set(Calendar.MINUTE, 59);
        end.set(Calendar.SECOND, 59);
        Timestamp setEndDate1 = new Timestamp(end.getTime().getTime());
        Date deleteEnd = new Date(setEndDate1.getTime());
        insertDataService.deleteData(deleteStart, deleteEnd);

        List<Map> getRow = fetchDataService.getRowData(setStartDate1, setEndDate1);
        Integer countRowData = (Integer) Integer.parseInt(getRow.get(0).get("count").toString());
        LOG.warn("Starting execute data with range : startDate : " + setStartDate1 + " endDate : " + setEndDate1);
        Integer staticLimit = 111;
        Integer sum1 = (countRowData.intValue() / staticLimit);
        int k = 0;

        for (int g = 0; g < staticLimit + 1; g++) {
            final Integer offset = (g * sum1);
            final Integer limit = sum1;
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    List<Map> dataList = fetchDataService.getRowData(setStartDate1, setEndDate1, limit, offset);
                    try {
                        List<TmnPrepaidDto> convert = fetchDataService.convertToLocalList(dataList);
                        insertDataService.insertAll(convert);
                    } catch (ParseException ex) {
                        LOG.error(ex);
                    } catch (SQLException ex) {
                        java.util.logging.Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });

        }
        executor.shutdown();
    }

}
