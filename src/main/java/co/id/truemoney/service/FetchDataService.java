package co.id.truemoney.service;

import co.id.truemoney.Dto.TmnPrepaidDto;
import co.id.truemoney.config.ConnectionConfig;
import co.id.truemoney.utils.ConvertUtils;
import co.id.truemoney.utils.NullChecker;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.javalite.activejdbc.Base;

public class FetchDataService extends ConnectionConfig {

    final static Logger LOG = Logger.getLogger(FetchDataService.class);

//    private static final String SQLPAGE = "select * from tmn_prepaid where systemdate between (?) and (?) limit (?) offset(?) ;";

    public List<Map> getRowData(Timestamp start, Timestamp end) {
        List<Map> countData = null;
        try {
            openFetch();
            countData = Base.findAll("\tSELECT \n" +
                    "\t\tcount(*) \n" +
                    "\tFROM \n" +
                    "\t\t\"TrStock\" A\n" +
                    "\tWHERE\n" +
                    "\t\tA.id_transaksi IN (SELECT \"TrTransaksi\".id_transaksi FROM  \"TrTransaksi\"  WHERE id_tipetransaksi IN (17, 18, 46, 67, 74) AND \"TimeStamp\"  BETWEEN (?) AND (?)) AND LENGTH (A.id_stock) < 14; \n", start, end);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeFetch();
        }
        return countData;
    }

    public List<Map> getRowData(Timestamp start, Timestamp end, Integer limit, Integer offset) {
        List<Map> data = new ArrayList<>();
        try {
            openFetch();
            LOG.info("finding query >>>>> " + "limit : " + limit + " offset : " + offset);
            data = Base.findAll(SQLPAGE, start, end, limit, offset);
            LOG.info("Retrieve Data >>>>> ");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeFetch();
        }
        return data;
    }

    public TmnPrepaidDto convertToLocal(Map fetch) throws ParseException {
        TmnPrepaidDto local = null;
        if (NullChecker.notNull(fetch)) {
            local = new TmnPrepaidDto();
            local.setId(Integer.valueOf(fetch.get("ID").toString()));
            local.setIdtransaksi(ConvertUtils.StringCheckIfNull(fetch.get("IDTRANSAKSI").toString()));
            String idTransaksiSuplier;
            if (fetch.get("IDTRANSAKSI_SUPPLIER") == null) {
                idTransaksiSuplier = null;
            } else {
                idTransaksiSuplier = fetch.get("IDTRANSAKSI_SUPPLIER").toString();
            }
            local.setIdtransaksiSupplier(idTransaksiSuplier);
            local.setTanggal(ConvertUtils.converDate(fetch.get("TANGGAL").toString()));
            local.setWaktu(ConvertUtils.converTime(fetch.get("WAKTU").toString()));
            String idAgent;
            if(fetch.get("ID_AGENT")==null){
                idAgent = null;
            }else{
                idAgent = fetch.get("ID_AGENT").toString();
            }
            local.setIdAgent(idAgent);
            String idAgentAccount;
            if(fetch.get("ID_AGENTACCOUNT")==null){
                idAgentAccount = null;
            }else{
                idAgentAccount = fetch.get("ID_AGENTACCOUNT").toString();
            }
            local.setIdAgentAccount(idAgentAccount);
            String idAgentCard;
            if(fetch.get("ID_AGENTCARD")==null){
                idAgentCard = null;
            }else{
                idAgentCard =fetch.get("ID_AGENTCARD").toString();
            }
            local.setIdAgentCard(idAgentCard);
            String namaAgent;
            if(fetch.get("NAMA_AGEN")==null){
                namaAgent = null;
            }else{
                namaAgent = fetch.get("NAMA_AGEN").toString();
            }
            local.setNamaAgent(namaAgent);
            String lokasi;
            if(fetch.get("LOKASI")==null){
                lokasi = null;
            }else {
                lokasi = fetch.get("LOKASI").toString();
            }
            local.setLokasi(lokasi);

            String idMember;
            if (fetch.get("ID_MEMBER") == (null)) {
                idMember = "";
            } else {
                idMember = ConvertUtils.StringCheckIfNull(fetch.get("ID_MEMBER").toString());
            }
            local.setIdMember(idMember);
            String stockType;
            if(fetch.get("STOCK_TYPE")==null){
                stockType = null;
            }else{
                stockType = fetch.get("STOCK_TYPE").toString();
            }
            local.setStockType(stockType);
            String channel;
            if(fetch.get("CHANNEL")==null){
                channel = null;
            }else{
                channel = fetch.get("CHANNEL").toString();
            }
            local.setChannel(channel);
            String subTipeTransaksi;
            if(fetch.get("SUB_TIPE_TRANSAKSI")==null){
                subTipeTransaksi = null;
            }else{
                subTipeTransaksi = fetch.get("SUB_TIPE_TRANSAKSI").toString();
            }
            local.setSubTipeTransaksi(subTipeTransaksi);
            String sn;
            if(fetch.get("SN")==null){
                sn = null;
            }else {
                sn = fetch.get("SN").toString();
            }
            local.setSn(sn);
            local.setStatusTraksaksi(ConvertUtils.StringCheckIfNull(fetch.get("STATUS_TRANSAKSI").toString()));
            local.setKeterangan(ConvertUtils.StringCheckIfNull(fetch.get("KETERANGAN").toString()));
            local.setTipeTransaksi(ConvertUtils.StringCheckIfNull(fetch.get("SUB_TIPE_TRANSAKSI").toString()));
            local.setNominal(Double.valueOf(fetch.get("NOMINAL").toString()));
            local.setNamaSupplier(ConvertUtils.StringCheckIfNull(fetch.get("NAMA_SUPPLIER").toString()));
            local.setNamaOperator(ConvertUtils.StringCheckIfNull(fetch.get("NAMA_OPERATOR").toString()));
            local.setHargaBeli(Double.valueOf(fetch.get("HARGABELI").toString()));
            local.setHargaJual(Double.valueOf(fetch.get("HARGAJUAL").toString()));
            local.setHargaCetakMemberOrAgent(Double.valueOf(fetch.get("HARGACETAK_MEMBER/AGEN").toString()));
            local.setRevenueFromTrueToMemberOrAgent(Double.valueOf(fetch.get("REVENUE_FROM_TRUE_TO_MEMBER/AGENT").toString()));
            local.setRevenueFromSupplierToTrue(Double.valueOf(fetch.get("REVENUE_FROM_SUPPLIER_TO_TRUE").toString()));
            local.setGrossRevenue(Double.valueOf(fetch.get("GROSS_REVENUE").toString()));
            local.setKomisiAgen(Double.valueOf(fetch.get("KOMISIAGEN").toString()));
            local.setKomisiDealer(Double.valueOf(fetch.get("KOMISIDEALER").toString()));
            local.setNetRevenue(Double.valueOf(fetch.get("NETREVENUE").toString()));
            local.setCashBack(Double.valueOf(fetch.get("CASHBACK").toString()));
            local.setNetProvit(Double.valueOf(fetch.get("NETPROVIT").toString()));
            local.setTimestampupdate(ConvertUtils.converTimeStamp(fetch.get("timestampupdate").toString()));
            Timestamp timesRefund;
            if (fetch.get("timestamprefund") == null) {
                timesRefund = null;
            } else {
                timesRefund = ConvertUtils.converTimeStamp(fetch.get("timestamprefund").toString());
            }
            local.setTimestamprefund(timesRefund);
            local.setFlagClean(Integer.valueOf(fetch.get("FLAG_CLEAN").toString()));
            Integer flagFinnance;
            if (fetch.get("FLAG_FINANCE") == null) {
                flagFinnance = null;
            } else {
                flagFinnance = Integer.valueOf(fetch.get("FLAG_FINANCE").toString());
            }
            local.setFlagFinnacne(flagFinnance);
            String noHpTujuan;
            if(fetch.get("NoHpTujuan")==null){
                noHpTujuan = null;
            }else{
                noHpTujuan = fetch.get("NoHpTujuan").toString();
            }
            local.setNoHpTujuan(noHpTujuan);
            Double hargaCetakFp;
            if(fetch.get("HARGA_CETAK_FP")==null){
                hargaCetakFp = null;
            }else {
                hargaCetakFp = Double.valueOf(fetch.get("HARGA_CETAK_FP").toString());
            }
            local.setHargaCetakFp(hargaCetakFp);
            Double potonganHrgFp;
            if(fetch.get("POTONGAN_HARGA_FP")==null){
                potonganHrgFp = null;
            }else{
                potonganHrgFp = Double.valueOf(fetch.get("POTONGAN_HARGA_FP").toString());
            }
            local.setPotonganHargaFp(potonganHrgFp);
            Double hrgCtkKurangHrgBeliFp;
            if(fetch.get("HARGA_CETAK-HARGA_BELI_FP")==null){
                hrgCtkKurangHrgBeliFp = null;
            }else {
                hrgCtkKurangHrgBeliFp = Double.valueOf(fetch.get("HARGA_CETAK-HARGA_BELI_FP").toString());
            }
            local.setHargaCetakKurangHargaBeliFp(hrgCtkKurangHrgBeliFp);
            Double grossRevenueFp;
            if(fetch.get("GROSS_REVENUE_FP")==null){
                grossRevenueFp = null;
            }else{
                grossRevenueFp = Double.valueOf(fetch.get("GROSS_REVENUE_FP").toString());
            }
            local.setGrossRevenueFp(grossRevenueFp);
            String tipeKomisi;
            if(fetch.get("tipekomisi")==null){
                tipeKomisi = null;
            }else{
                tipeKomisi = fetch.get("tipekomisi").toString();
            }
            local.setTipeKomisi(tipeKomisi);
            Double total;
            if(fetch.get("TOTAL")==null){
                total = null;
            }else{
                total=Double.valueOf(fetch.get("TOTAL").toString());
            }
            local.setTotal(total);
            Double hargaCetakPromo;
            if(fetch.get("hargacetakpromo")==null){
                hargaCetakPromo = null;
            }else {
                hargaCetakPromo =Double.valueOf(fetch.get("hargacetakpromo").toString());
            }
            local.setHargaCetakPromo(hargaCetakPromo);
            Double promo;
            if(fetch.get("PROMO")==null){
                promo = null;
            }else{
                promo = Double.valueOf(fetch.get("PROMO").toString());
            }
            local.setPromo(promo);
            String promoType;
            if(fetch.get("PROMOTYPE")==null){
                promoType = null;
            }else {
                promoType = fetch.get("PROMOTYPE").toString();
            }
            local.setPromoType(promoType);
            Double bonus;
            if(fetch.get("BONUS")==null){
                bonus = null;
            }else{
                bonus = Double.valueOf(fetch.get("BONUS").toString());
            }
            local.setBonus(bonus);
            String idPellanggan;
            if(fetch.get("ID_PELANGGAN")==null){
                idPellanggan = null;
            }else{
                idPellanggan=fetch.get("ID_PELANGGAN").toString();
            }
            local.setIdPelanggan(idPellanggan);
            String applicantFullName;
            if(fetch.get("applicant_full_name")==null){
                applicantFullName = null;
            }else {
                applicantFullName = fetch.get("applicant_full_name").toString();
            }
            local.setNamaApplicant(applicantFullName);
            String applicantId;
            if(fetch.get("applicant_national_id")==null){
                applicantId = null;
            }else{
                applicantId = fetch.get("applicant_national_id").toString();
            }
            local.setApplicantNationalId(applicantId);
            String transactionGateway;
            if(fetch.get("transactionIdGateway")==null){
                transactionGateway = null;
            }else{
                transactionGateway = fetch.get("transactionIdGateway").toString();
            }
            local.setTransactionIdGateway(transactionGateway);
            java.util.Date now = new java.util.Date();
            local.setImporteddate(new Timestamp(now.getTime()));
        }
        return local;
    }

    public List<TmnPrepaidDto> convertToLocalList(List<Map> modelList) throws ParseException {
        List<TmnPrepaidDto> result = null;
        if (NullChecker.notNull(modelList)) {
            result = new ArrayList<>();
            for (Map model : modelList) {
                TmnPrepaidDto dto = convertToLocal(model);
                result.add(dto);
            }
        }
        return result;
    }

    private static final String SQLPAGE = "WITH T0 AS (\n" +
            "\tSELECT\n" +
            "\t\t*\n" +
            "\tFROM\n" +
            "\t\t\"TrStock\" A\n" +
            "\tWHERE\n" +
            "\t\tA .id_transaksi IN (\n" +
            "\t\t\tSELECT\n" +
            "\t\t\t\t\"TrTransaksi\".id_transaksi\n" +
            "\t\t\tFROM\n" +
            "\t\t\t\t\"TrTransaksi\"\n" +
            "\t\t\tWHERE\n" +
            "\t\t\t\tid_tipetransaksi IN (17, 18, 46, 67, 74)\n" +
            "\t\t\tAND \"TimeStamp\"  BETWEEN (?) AND (?) Limit (?) offset (?)  \n" +
            "\t\t)\n" +
            "\t\tAND LENGTH (A .id_stock) < 14\n" +
            "),\n" +
            " TT AS (\n" +
            "\tSELECT DISTINCT\n" +
            "\t\tT0.id_transaksi\n" +
            "\tFROM\n" +
            "\t\tT0\n" +
            "),\n" +
            " TA AS (\n" +
            "\tSELECT\n" +
            "\t\tT0.id_transaksi,\n" +
            "\t\tMAX (T0.\"Nominal\") AS \"MADA\"\n" +
            "\tFROM\n" +
            "\t\tT0\n" +
            "\tWHERE\n" +
            "\t\tUPPER (\n" +
            "\t\t\tSUBSTRING (T0.\"StokType\", 1, 4)\n" +
            "\t\t) = 'AGEN'\n" +
            "\tAND T0.\"TypeTrx\" = 'DEPOSIT'\n" +
            "\tGROUP BY\n" +
            "\t\tT0.id_transaksi\n" +
            "),\n" +
            " TB AS (\n" +
            "\tSELECT\n" +
            "\t\tT0.id_transaksi,\n" +
            "\t\tMIN (T0.\"Nominal\") AS \"MIDA\"\n" +
            "\tFROM\n" +
            "\t\tT0\n" +
            "\tWHERE\n" +
            "\t\tUPPER (\n" +
            "\t\t\tSUBSTRING (T0.\"StokType\", 1, 4)\n" +
            "\t\t) = 'AGEN'\n" +
            "\tAND T0.\"TypeTrx\" = 'DEPOSIT'\n" +
            "\tGROUP BY\n" +
            "\t\tT0.id_transaksi\n" +
            "),\n" +
            " TC AS (\n" +
            "\tSELECT\n" +
            "\t\tT0.id_transaksi,\n" +
            "\t\tMAX (T0.\"Nominal\") AS \"MAWA\"\n" +
            "\tFROM\n" +
            "\t\tT0\n" +
            "\tWHERE\n" +
            "\t\tUPPER (\n" +
            "\t\t\tSUBSTRING (T0.\"StokType\", 1, 4)\n" +
            "\t\t) = 'AGEN'\n" +
            "\tAND T0.\"TypeTrx\" = 'WITHDRAW'\n" +
            "\tGROUP BY\n" +
            "\t\tT0.id_transaksi\n" +
            "),\n" +
            " TD AS (\n" +
            "\tSELECT\n" +
            "\t\tT0.id_transaksi,\n" +
            "\t\tMIN (T0.\"Nominal\") AS \"MIWA\"\n" +
            "\tFROM\n" +
            "\t\tT0\n" +
            "\tWHERE\n" +
            "\t\tUPPER (\n" +
            "\t\t\tSUBSTRING (T0.\"StokType\", 1, 4)\n" +
            "\t\t) = 'AGEN'\n" +
            "\tAND T0.\"TypeTrx\" = 'WITHDRAW'\n" +
            "\tGROUP BY\n" +
            "\t\tT0.id_transaksi\n" +
            "),\n" +
            " TE AS (\n" +
            "\tSELECT\n" +
            "\t\tT0.id_transaksi,\n" +
            "\t\tMAX (T0.\"Nominal\") AS \"MADT\"\n" +
            "\tFROM\n" +
            "\t\tT0\n" +
            "\tWHERE\n" +
            "\t\tUPPER (\n" +
            "\t\t\tSUBSTRING (T0.\"StokType\", 1, 4)\n" +
            "\t\t) = 'TRUE'\n" +
            "\tAND T0.\"TypeTrx\" = 'DEPOSIT'\n" +
            "\tGROUP BY\n" +
            "\t\tT0.id_transaksi\n" +
            "),\n" +
            " TF AS (\n" +
            "\tSELECT\n" +
            "\t\tT0.id_transaksi,\n" +
            "\t\tMIN (T0.\"Nominal\") AS \"MIDT\"\n" +
            "\tFROM\n" +
            "\t\tT0\n" +
            "\tWHERE\n" +
            "\t\tUPPER (\n" +
            "\t\t\tSUBSTRING (T0.\"StokType\", 1, 4)\n" +
            "\t\t) = 'TRUE'\n" +
            "\tAND T0.\"TypeTrx\" = 'DEPOSIT'\n" +
            "\tGROUP BY\n" +
            "\t\tT0.id_transaksi\n" +
            "),\n" +
            " TG AS (\n" +
            "\tSELECT\n" +
            "\t\tT0.id_transaksi,\n" +
            "\t\tMAX (T0.\"Nominal\") AS \"MAWT\"\n" +
            "\tFROM\n" +
            "\t\tT0\n" +
            "\tWHERE\n" +
            "\t\tUPPER (\n" +
            "\t\t\tSUBSTRING (T0.\"StokType\", 1, 4)\n" +
            "\t\t) = 'TRUE'\n" +
            "\tAND T0.\"TypeTrx\" = 'WITHDRAW'\n" +
            "\tGROUP BY\n" +
            "\t\tT0.id_transaksi\n" +
            "),\n" +
            " TH AS (\n" +
            "\tSELECT\n" +
            "\t\tT0.id_transaksi,\n" +
            "\t\tMIN (T0.\"Nominal\") AS \"MIWT\"\n" +
            "\tFROM\n" +
            "\t\tT0\n" +
            "\tWHERE\n" +
            "\t\tUPPER (\n" +
            "\t\t\tSUBSTRING (T0.\"StokType\", 1, 4)\n" +
            "\t\t) = 'TRUE'\n" +
            "\tAND T0.\"TypeTrx\" = 'WITHDRAW'\n" +
            "\tGROUP BY\n" +
            "\t\tT0.id_transaksi\n" +
            "),\n" +
            " TI AS (\n" +
            "\tSELECT\n" +
            "\t\tT0.id_transaksi,\n" +
            "\t\tMAX (T0.\"Nominal\") AS \"MADM\"\n" +
            "\tFROM\n" +
            "\t\tT0\n" +
            "\tWHERE\n" +
            "\t\tUPPER (\n" +
            "\t\t\tSUBSTRING (T0.\"StokType\", 1, 4)\n" +
            "\t\t) = 'MEMB'\n" +
            "\tAND T0.\"TypeTrx\" = 'DEPOSIT'\n" +
            "\tGROUP BY\n" +
            "\t\tT0.id_transaksi\n" +
            "),\n" +
            " TJ AS (\n" +
            "\tSELECT\n" +
            "\t\tT0.id_transaksi,\n" +
            "\t\tMIN (T0.\"Nominal\") AS \"MIDM\"\n" +
            "\tFROM\n" +
            "\t\tT0\n" +
            "\tWHERE\n" +
            "\t\tUPPER (\n" +
            "\t\t\tSUBSTRING (T0.\"StokType\", 1, 4)\n" +
            "\t\t) = 'MEMB'\n" +
            "\tAND T0.\"TypeTrx\" = 'DEPOSIT'\n" +
            "\tGROUP BY\n" +
            "\t\tT0.id_transaksi\n" +
            "),\n" +
            " TK AS (\n" +
            "\tSELECT\n" +
            "\t\tT0.id_transaksi,\n" +
            "\t\tMAX (T0.\"Nominal\") AS \"MAWM\"\n" +
            "\tFROM\n" +
            "\t\tT0\n" +
            "\tWHERE\n" +
            "\t\tUPPER (\n" +
            "\t\t\tSUBSTRING (T0.\"StokType\", 1, 4)\n" +
            "\t\t) = 'MEMB'\n" +
            "\tAND T0.\"TypeTrx\" = 'WITHDRAW'\n" +
            "\tGROUP BY\n" +
            "\t\tT0.id_transaksi\n" +
            "),\n" +
            " TL AS (\n" +
            "\tSELECT\n" +
            "\t\tT0.id_transaksi,\n" +
            "\t\tMIN (T0.\"Nominal\") AS \"MIWM\"\n" +
            "\tFROM\n" +
            "\t\tT0\n" +
            "\tWHERE\n" +
            "\t\tUPPER (\n" +
            "\t\t\tSUBSTRING (T0.\"StokType\", 1, 4)\n" +
            "\t\t) = 'MEMB'\n" +
            "\tAND T0.\"TypeTrx\" = 'WITHDRAW'\n" +
            "\tGROUP BY\n" +
            "\t\tT0.id_transaksi\n" +
            "),\n" +
            " TM AS (\n" +
            "\tSELECT\n" +
            "\t\tT0.id_transaksi,\n" +
            "\t\tMIN (T0.\"Nominal\") AS \"WS\"\n" +
            "\tFROM\n" +
            "\t\tT0\n" +
            "\tWHERE\n" +
            "\t\tUPPER (\n" +
            "\t\t\tSUBSTRING (T0.\"StokType\", 1, 4)\n" +
            "\t\t) = 'SUPP'\n" +
            "\tAND T0.\"TypeTrx\" = 'WITHDRAW'\n" +
            "\tGROUP BY\n" +
            "\t\tT0.id_transaksi\n" +
            "),\n" +
            " TM2 AS (\n" +
            "\tSELECT\n" +
            "\t\tT0.id_transaksi,\n" +
            "\t\tMIN (T0.\"Nominal\") AS \"DD\"\n" +
            "\tFROM\n" +
            "\t\tT0\n" +
            "\tWHERE\n" +
            "\t\tUPPER (\n" +
            "\t\t\tSUBSTRING (T0.\"StokType\", 1, 4)\n" +
            "\t\t) = 'DEAL'\n" +
            "\tAND T0.\"TypeTrx\" = 'DEPOSIT'\n" +
            "\tGROUP BY\n" +
            "\t\tT0.id_transaksi\n" +
            "),\n" +
            " TN AS (\n" +
            "\tSELECT\n" +
            "\t\tTT.id_transaksi,\n" +
            "\t\tCASE\n" +
            "\tWHEN TA.\"MADA\" IS NULL THEN\n" +
            "\t\t0\n" +
            "\tELSE\n" +
            "\t\tTA.\"MADA\"\n" +
            "\tEND AS \"MADA\",\n" +
            "\tCASE\n" +
            "WHEN TB.\"MIDA\" IS NULL THEN\n" +
            "\t0\n" +
            "ELSE\n" +
            "\tTB.\"MIDA\"\n" +
            "END AS \"MIDA\",\n" +
            " CASE\n" +
            "WHEN TC.\"MAWA\" IS NULL THEN\n" +
            "\t0\n" +
            "ELSE\n" +
            "\tTC.\"MAWA\"\n" +
            "END AS \"MAWA\",\n" +
            " CASE\n" +
            "WHEN TD.\"MIWA\" IS NULL THEN\n" +
            "\t0\n" +
            "ELSE\n" +
            "\tTD.\"MIWA\"\n" +
            "END AS \"MIWA\",\n" +
            " CASE\n" +
            "WHEN TE.\"MADT\" IS NULL THEN\n" +
            "\t0\n" +
            "ELSE\n" +
            "\tTE.\"MADT\"\n" +
            "END AS \"MADT\",\n" +
            " CASE\n" +
            "WHEN TF.\"MIDT\" IS NULL THEN\n" +
            "\t0\n" +
            "ELSE\n" +
            "\tTF.\"MIDT\"\n" +
            "END AS \"MIDT\",\n" +
            " CASE\n" +
            "WHEN TG.\"MAWT\" IS NULL THEN\n" +
            "\t0\n" +
            "ELSE\n" +
            "\tTG.\"MAWT\"\n" +
            "END AS \"MAWT\",\n" +
            " CASE\n" +
            "WHEN TH.\"MIWT\" IS NULL THEN\n" +
            "\t0\n" +
            "ELSE\n" +
            "\tTH.\"MIWT\"\n" +
            "END AS \"MIWT\",\n" +
            " CASE\n" +
            "WHEN TI.\"MADM\" IS NULL THEN\n" +
            "\t0\n" +
            "ELSE\n" +
            "\tTI.\"MADM\"\n" +
            "END AS \"MADM\",\n" +
            " CASE\n" +
            "WHEN TJ.\"MIDM\" IS NULL THEN\n" +
            "\t0\n" +
            "ELSE\n" +
            "\tTJ.\"MIDM\"\n" +
            "END AS \"MIDM\",\n" +
            " CASE\n" +
            "WHEN TK.\"MAWM\" IS NULL THEN\n" +
            "\t0\n" +
            "ELSE\n" +
            "\tTK.\"MAWM\"\n" +
            "END AS \"MAWM\",\n" +
            " CASE\n" +
            "WHEN TL.\"MIWM\" IS NULL THEN\n" +
            "\t0\n" +
            "ELSE\n" +
            "\tTL.\"MIWM\"\n" +
            "END AS \"MIWM\",\n" +
            " CASE\n" +
            "WHEN TM.\"WS\" IS NULL THEN\n" +
            "\t0\n" +
            "ELSE\n" +
            "\tTM.\"WS\"\n" +
            "END AS \"WS\",\n" +
            " CASE\n" +
            "WHEN TM2.\"DD\" IS NULL THEN\n" +
            "\t0\n" +
            "ELSE\n" +
            "\tTM2.\"DD\"\n" +
            "END AS \"DD\"\n" +
            "FROM\n" +
            "\tTT\n" +
            "FULL OUTER JOIN TA ON TT.id_transaksi = TA.id_transaksi\n" +
            "FULL OUTER JOIN TB ON TT.id_transaksi = TB.id_transaksi\n" +
            "FULL OUTER JOIN TC ON TT.id_transaksi = TC.id_transaksi\n" +
            "FULL OUTER JOIN TD ON TT.id_transaksi = TD.id_transaksi\n" +
            "FULL OUTER JOIN TE ON TT.id_transaksi = TE.id_transaksi\n" +
            "FULL OUTER JOIN TF ON TT.id_transaksi = TF.id_transaksi\n" +
            "FULL OUTER JOIN TG ON TT.id_transaksi = TG.id_transaksi\n" +
            "FULL OUTER JOIN TH ON TT.id_transaksi = TH.id_transaksi\n" +
            "FULL OUTER JOIN TI ON TT.id_transaksi = TI.id_transaksi\n" +
            "FULL OUTER JOIN TJ ON TT.id_transaksi = TJ.id_transaksi\n" +
            "FULL OUTER JOIN TK ON TT.id_transaksi = TK.id_transaksi\n" +
            "FULL OUTER JOIN TL ON TT.id_transaksi = TL.id_transaksi\n" +
            "FULL OUTER JOIN TM ON TT.id_transaksi = TM.id_transaksi\n" +
            "FULL OUTER JOIN TM2 ON TT.id_transaksi = TM2.id_transaksi\n" +
            "),\n" +
            " TP AS (\n" +
            "\tSELECT\n" +
            "\t\tTN.\"id_transaksi\",\n" +
            "\t\tA .id_trx AS \"IDTRANSAKSI\",\n" +
            "\t\tA .id_trxsupplier AS \"IDTRANSAKSI_SUPPLIER\",\n" +
            "\t\tA .id_agentaccount,\n" +
            "\t\tA .id_memberaccount,\n" +
            "\t\tA .id_supplier,\n" +
            "\t\tTN.\"MADA\",\n" +
            "\t\tTN.\"MIDA\",\n" +
            "\t\tTN.\"MAWA\",\n" +
            "\t\tTN.\"MIWA\",\n" +
            "\t\tTN.\"MADT\",\n" +
            "\t\tTN.\"MIDT\",\n" +
            "\t\tTN.\"MAWT\",\n" +
            "\t\tTN.\"MIWT\",\n" +
            "\t\tTN.\"MADM\",\n" +
            "\t\tTN.\"MIDM\",\n" +
            "\t\tTN.\"MAWM\",\n" +
            "\t\tTN.\"MIWM\",\n" +
            "\t\tTN.\"WS\",\n" +
            "\t\tTN.\"DD\",\n" +
            "\t\tA .\"Keterangan\",\n" +
            "\t\tA .\"TimeStamp\" AS \"WAKTU\",\n" +
            "\t\tA .id_tipetransaksi,\n" +
            "\t\tA .\"Nominal\",\n" +
            "\t\tA .\"StatusDana\",\n" +
            "\t\tB.\"NamaTipeAplikasi\",\n" +
            "\t\tA .\"CustomerReference\",\n" +
            "\t\tC .\"NamaOperator\",\n" +
            "\t\tA .\"StatusTRX\",\n" +
            "\t\tA .timestampupdatetrx,\n" +
            "\t\tA .timestamprefund,\n" +
            "\t\tCASE\n" +
            "\tWHEN A .id_tipetransaksi = '67' THEN\n" +
            "\t\tCASE\n" +
            "\tWHEN A .hargabeli IS NULL THEN\n" +
            "\t\t0\n" +
            "\tELSE\n" +
            "\t\tA .\"Total\" - A .\"Biaya\" :: INT\n" +
            "\tEND\n" +
            "\tELSE\n" +
            "\t\tCASE\n" +
            "\tWHEN A .hargabeli IS NULL THEN\n" +
            "\t\t0\n" +
            "\tELSE\n" +
            "\t\tA .hargabeli\n" +
            "\tEND\n" +
            "\tEND AS hargabeli,\n" +
            "\tCASE\n" +
            "WHEN A .hargajual IS NULL THEN\n" +
            "\t0\n" +
            "ELSE\n" +
            "\tA .hargajual\n" +
            "END AS hargajual,\n" +
            " A .\"NoHPTujuan\",\n" +
            " A .id_feesupplier,\n" +
            " A .id_pelanggan\n" +
            "FROM\n" +
            "\tTN\n" +
            "JOIN \"TrTransaksi\" A ON TN.id_transaksi = A .id_transaksi\n" +
            "LEFT OUTER JOIN \"MsTipeAplikasi\" B ON A .id_tipeaplikasi = B.id_tipeaplikasi\n" +
            "JOIN \"MsOperator\" C ON A .id_operator = C .id_operator\n" +
            "),\n" +
            " TIA AS (\n" +
            "\tSELECT\n" +
            "\t\t\"MsInformasiAgent\".\"Nama\",\n" +
            "\t\t\"MsInformasiAgent\".\"id_agent\"\n" +
            "\tFROM\n" +
            "\t\t\"MsInformasiAgent\"\n" +
            "\tWHERE\n" +
            "\t\t\"MsInformasiAgent\".\"Nama\" != ''\n" +
            "\tAND \"MsInformasiAgent\".\"id_informasiagent\" IN (\n" +
            "\t\tSELECT\n" +
            "\t\t\tMIN (\n" +
            "\t\t\t\t\"MsInformasiAgent\".\"id_informasiagent\"\n" +
            "\t\t\t)\n" +
            "\t\tFROM\n" +
            "\t\t\t\"MsInformasiAgent\"\n" +
            "\t\tWHERE\n" +
            "\t\t\t\"MsInformasiAgent\".\"id_agent\" IS NOT NULL\n" +
            "\t\tGROUP BY\n" +
            "\t\t\t\"MsInformasiAgent\".\"id_agent\"\n" +
            "\t)\n" +
            "),\n" +
            " TAA AS (\n" +
            "\tSELECT\n" +
            "\t\tA .id_agentaccount,\n" +
            "\t\tA .id_agent,\n" +
            "\t\tA .\"NomorKartu\",\n" +
            "\t\tTIA.\"Nama\",\n" +
            "\t\tA .id_cabang\n" +
            "\tFROM\n" +
            "\t\t\"MsAgentAccount\" A\n" +
            "\tJOIN TIA ON A .\"id_agent\" = TIA.\"id_agent\"\n" +
            "),\n" +
            " TZ AS (\n" +
            "\tSELECT\n" +
            "\t\tTP.id_transaksi,\n" +
            "\t\tTP.\"WAKTU\" :: DATE AS \"TANGGAL\",\n" +
            "\t\tTP.\"WAKTU\" :: TIME AS \"WAKTU\",\n" +
            "\t\tTP.\"WAKTU\" AS \"timestamp\",\n" +
            "\t\tTP.\"IDTRANSAKSI\",\n" +
            "\t\tTP.\"IDTRANSAKSI_SUPPLIER\",\n" +
            "\t\tTAA.id_agent,\n" +
            "\t\tTP.\"id_agentaccount\",\n" +
            "\t\tTAA.\"NomorKartu\",\n" +
            "\t\tTAA.\"Nama\",\n" +
            "\t\tTP.id_memberaccount,\n" +
            "\t\tTAA.id_cabang,\n" +
            "\t\tTP.\"NamaTipeAplikasi\",\n" +
            "\t\tCASE\n" +
            "\tWHEN TP.id_memberaccount = ''\n" +
            "\tAND TP.id_agentaccount IS NOT NULL THEN\n" +
            "\t\t'AGENT'\n" +
            "\tWHEN TP.id_memberaccount <> ''\n" +
            "\tAND TP.id_agentaccount IS NOT NULL THEN\n" +
            "\t\t'MTA'\n" +
            "\tWHEN TP.id_memberaccount IS NOT NULL\n" +
            "\tAND TP.id_agentaccount IS NOT NULL THEN\n" +
            "\t\t'MTA'\n" +
            "\tWHEN TP.id_memberaccount <> '' THEN\n" +
            "\t\t'MEMBER'\n" +
            "\tWHEN TP.id_memberaccount IS NOT NULL THEN\n" +
            "\t\t'MEMBER'\n" +
            "\tELSE\n" +
            "\t\t'AGENT'\n" +
            "\tEND AS \"STOCK_TYPE\",\n" +
            "\tTP.\"Keterangan\",\n" +
            "\tB.\"NamaTipeTransaksi\",\n" +
            "\tTP.\"Nominal\",\n" +
            "\tA .\"NamaSupplier\",\n" +
            "\tTP.\"CustomerReference\",\n" +
            "\tTP.\"NamaOperator\",\n" +
            "\tTP.\"StatusTRX\",\n" +
            "\tTP.timestampupdatetrx,\n" +
            "\tTP.timestamprefund,\n" +
            "\tTP.\"MADA\",\n" +
            "\tTP.\"MIDA\",\n" +
            "\tTP.\"MAWA\",\n" +
            "\tTP.\"MIWA\",\n" +
            "\tTP.\"MADT\",\n" +
            "\tTP.\"MIDT\",\n" +
            "\tTP.\"MAWT\",\n" +
            "\tTP.\"MIWT\",\n" +
            "\tTP.\"MADM\",\n" +
            "\tTP.\"MIDM\",\n" +
            "\tTP.\"MAWM\",\n" +
            "\tTP.\"MIWM\",\n" +
            "\tTP.\"WS\",\n" +
            "\tTP.\"DD\",\n" +
            "\tCASE\n" +
            "WHEN TP.hargabeli = 0 THEN\n" +
            "\tTP.\"WS\"\n" +
            "ELSE\n" +
            "\tTP.hargabeli\n" +
            "END AS \"HARGABELI\",\n" +
            " CASE\n" +
            "WHEN TP.hargajual = 0 THEN\n" +
            "\tTP.\"MADT\"\n" +
            "ELSE\n" +
            "\tTP.hargajual\n" +
            "END AS \"HARGAJUAL\",\n" +
            " CASE\n" +
            "WHEN TP.\"MAWM\" = 0 THEN\n" +
            "\tTP.\"MAWA\"\n" +
            "ELSE\n" +
            "\tTP.\"MAWM\"\n" +
            "END AS \"HARGACETAK_MEMBER/AGEN\",\n" +
            " CASE\n" +
            "WHEN TP.\"MAWT\" = 0\n" +
            "AND TP.\"MAWA\" = 0 THEN\n" +
            "\t'DIAMBIL DARI WITHDRAW MEMBER'\n" +
            "WHEN TP.\"MAWT\" = 0\n" +
            "AND TP.\"MAWM\" = 0 THEN\n" +
            "\t'DIAMBIL DARI WITHDRAW AGEN'\n" +
            "WHEN TP.\"MAWA\" = 0\n" +
            "AND TP.\"MAWM\" = 0 THEN\n" +
            "\t'DIAMBIL DARI DEPOSIT TRUE'\n" +
            "ELSE\n" +
            "\t'DIAMBIL DARI WITHDRAW MEMBER'\n" +
            "END AS \"REMARKS_HARGA_CETAK\",\n" +
            " CASE\n" +
            "WHEN TP.\"MADT\" < TP.\"WS\" THEN\n" +
            "\t'DEPOSIT TRUE SALAH,DIAMBIL DARI WITHDRAW SUPPLIER'\n" +
            "ELSE\n" +
            "\t'DEPOSIT TRUE BENAR'\n" +
            "END AS \"REMARKS_HARGA_JUAL\",\n" +
            " TP.\"NoHPTujuan\",\n" +
            " C .\"HargaCetakMember\",\n" +
            " C .\"TrueFeeType\",\n" +
            " C .\"flagActivePromo\",\n" +
            " C .\"flagActivePromoMember\",\n" +
            " C .\"id_promo\",\n" +
            " C .\"id_promoMember\",\n" +
            " TP.\"id_pelanggan\"\n" +
            "FROM\n" +
            "\tTP\n" +
            "LEFT OUTER JOIN TAA ON TP.\"id_agentaccount\" = TAA.id_agentaccount\n" +
            "LEFT OUTER JOIN \"MsSupplier\" A ON TP.\"id_supplier\" = A .id_supplier\n" +
            "LEFT OUTER JOIN \"MsTipeTransaksi\" B ON TP.id_tipetransaksi = B.id_tipetransaksi\n" +
            "LEFT OUTER JOIN \"MsFeeSupplier\" C ON TP.id_feesupplier = C .id_feesupplier\n" +
            "),\n" +
            " TZ1 AS (\n" +
            "\tSELECT\n" +
            "\t\tTZ.id_transaksi,\n" +
            "\t\tTZ.\"TANGGAL\",\n" +
            "\t\tTZ.\"WAKTU\",\n" +
            "\t\tTZ.\"IDTRANSAKSI\",\n" +
            "\t\tTZ.\"IDTRANSAKSI_SUPPLIER\",\n" +
            "\t\tTZ.id_agent,\n" +
            "\t\tTZ.\"id_agentaccount\",\n" +
            "\t\tTZ.\"NomorKartu\",\n" +
            "\t\tTZ.\"Nama\",\n" +
            "\t\tTZ.id_memberaccount,\n" +
            "\t\tTZ.\"STOCK_TYPE\",\n" +
            "\t\tTZ.\"Keterangan\",\n" +
            "\t\tTZ.\"NamaTipeTransaksi\",\n" +
            "\t\tTZ.\"Nominal\",\n" +
            "\t\tTZ.\"timestamp\",\n" +
            "\t\tTZ.\"NamaSupplier\",\n" +
            "\t\tTZ.\"CustomerReference\",\n" +
            "\t\tTZ.\"StatusTRX\",\n" +
            "\t\tTZ.timestampupdatetrx,\n" +
            "\t\tTZ.timestamprefund,\n" +
            "\t\tTZ.\"MADA\",\n" +
            "\t\tTZ.\"MIDA\",\n" +
            "\t\tTZ.\"MAWA\",\n" +
            "\t\tTZ.\"MIWA\",\n" +
            "\t\tTZ.\"MADT\",\n" +
            "\t\tTZ.\"MIDT\",\n" +
            "\t\tTZ.\"MAWT\",\n" +
            "\t\tTZ.\"MIWT\",\n" +
            "\t\tTZ.\"MADM\",\n" +
            "\t\tTZ.\"MIDM\",\n" +
            "\t\tTZ.\"MAWM\",\n" +
            "\t\tTZ.\"MIWM\",\n" +
            "\t\tTZ.\"WS\",\n" +
            "\t\tTZ.\"HARGABELI\",\n" +
            "\t\tCASE\n" +
            "\tWHEN TZ.\"NamaTipeTransaksi\" = 'Pulsa Prabayar' THEN\n" +
            "\t\tCASE\n" +
            "\tWHEN TZ.\"HARGAJUAL\" < TZ.\"HARGABELI\" THEN\n" +
            "\t\tCASE\n" +
            "\tWHEN TZ.\"STOCK_TYPE\" = 'AGENT' THEN\n" +
            "\t\tCASE\n" +
            "\tWHEN TZ.\"flagActivePromo\" = 't' THEN\n" +
            "\t\tCASE\n" +
            "\tWHEN TZ.\"id_promo\" IS NOT NULL THEN\n" +
            "\t\t(\n" +
            "\t\t\tSELECT\n" +
            "\t\t\t\tCASE\n" +
            "\t\t\tWHEN TZ.\"timestamp\" :: DATE BETWEEN X.tgl_mulai\n" +
            "\t\t\tAND X.tgl_selesai THEN\n" +
            "\t\t\t\tCASE\n" +
            "\t\t\tWHEN X.promo_type = 'CASHBACK' THEN\n" +
            "\t\t\t\tTZ.\"HARGAJUAL\" + CAST (X.promo_amount AS INTEGER)\n" +
            "\t\t\tWHEN X.promo_type = 'DISCOUNT' THEN\n" +
            "\t\t\t\tROUND(\n" +
            "\t\t\t\t\t(TZ.\"HARGAJUAL\" * 10) / (\n" +
            "\t\t\t\t\t\t10 - (\n" +
            "\t\t\t\t\t\t\tCAST (X.promo_amount AS INTEGER) * 0.1\n" +
            "\t\t\t\t\t\t)\n" +
            "\t\t\t\t\t)\n" +
            "\t\t\t\t)\n" +
            "\t\t\tELSE\n" +
            "\t\t\t\tTZ.\"HARGAJUAL\"\n" +
            "\t\t\tEND\n" +
            "\t\t\tELSE\n" +
            "\t\t\t\tTZ.\"HARGAJUAL\"\n" +
            "\t\t\tEND\n" +
            "\t\t\tFROM\n" +
            "\t\t\t\t\"MsPromo\" X\n" +
            "\t\t\tWHERE\n" +
            "\t\t\t\tX.id_promo = TZ.id_promo\n" +
            "\t\t)\n" +
            "\tELSE\n" +
            "\t\tTZ.\"HARGAJUAL\"\n" +
            "\tEND\n" +
            "\tELSE\n" +
            "\t\tTZ.\"HARGAJUAL\"\n" +
            "\tEND\n" +
            "\tWHEN TZ.\"STOCK_TYPE\" = 'MEMBER' THEN\n" +
            "\t\tCASE\n" +
            "\tWHEN TZ.\"flagActivePromoMember\" = 't' THEN\n" +
            "\t\tCASE\n" +
            "\tWHEN TZ.\"id_promoMember\" IS NOT NULL THEN\n" +
            "\t\t(\n" +
            "\t\t\tSELECT\n" +
            "\t\t\t\tCASE\n" +
            "\t\t\tWHEN TZ.\"timestamp\" :: DATE BETWEEN X.tgl_mulai\n" +
            "\t\t\tAND X.tgl_selesai THEN\n" +
            "\t\t\t\tCASE\n" +
            "\t\t\tWHEN X.promo_type = 'CASHBACK' THEN\n" +
            "\t\t\t\tTZ.\"HARGAJUAL\" + CAST (X.promo_amount AS INTEGER)\n" +
            "\t\t\tWHEN X.promo_type = 'DISCOUNT' THEN\n" +
            "\t\t\t\tROUND(\n" +
            "\t\t\t\t\t(TZ.\"HARGAJUAL\" * 10) / (\n" +
            "\t\t\t\t\t\t10 - (\n" +
            "\t\t\t\t\t\t\tCAST (X.promo_amount AS INTEGER) * 0.1\n" +
            "\t\t\t\t\t\t)\n" +
            "\t\t\t\t\t)\n" +
            "\t\t\t\t)\n" +
            "\t\t\tELSE\n" +
            "\t\t\t\tTZ.\"HARGAJUAL\"\n" +
            "\t\t\tEND\n" +
            "\t\t\tELSE\n" +
            "\t\t\t\tTZ.\"HARGAJUAL\"\n" +
            "\t\t\tEND\n" +
            "\t\t\tFROM\n" +
            "\t\t\t\t\"MsPromo\" X\n" +
            "\t\t\tWHERE\n" +
            "\t\t\t\tX.id_promo = TZ.\"id_promoMember\"\n" +
            "\t\t)\n" +
            "\tELSE\n" +
            "\t\tTZ.\"HARGAJUAL\"\n" +
            "\tEND\n" +
            "\tELSE\n" +
            "\t\tTZ.\"HARGAJUAL\"\n" +
            "\tEND\n" +
            "\tWHEN TZ.\"STOCK_TYPE\" = 'MTA' THEN\n" +
            "\t\tTZ.\"HARGAJUAL\"\n" +
            "\tEND\n" +
            "\tELSE\n" +
            "\t\tTZ.\"HARGAJUAL\"\n" +
            "\tEND\n" +
            "\tELSE\n" +
            "\t\tTZ.\"HARGAJUAL\"\n" +
            "\tEND AS \"HARGAJUAL\",\n" +
            "\tCASE\n" +
            "WHEN TZ.\"NamaTipeTransaksi\" = 'Pulsa Prabayar' THEN\n" +
            "\tCASE\n" +
            "WHEN TZ.\"HARGAJUAL\" < TZ.\"HARGABELI\" THEN\n" +
            "\tCASE\n" +
            "WHEN TZ.\"STOCK_TYPE\" = 'AGENT' THEN\n" +
            "\tCASE\n" +
            "WHEN TZ.\"flagActivePromo\" = 't' THEN\n" +
            "\tCASE\n" +
            "WHEN TZ.\"id_promo\" IS NOT NULL THEN\n" +
            "\t(\n" +
            "\t\tSELECT\n" +
            "\t\t\tCASE\n" +
            "\t\tWHEN TZ.\"timestamp\" :: DATE BETWEEN X.tgl_mulai\n" +
            "\t\tAND X.tgl_selesai THEN\n" +
            "\t\t\tCASE\n" +
            "\t\tWHEN X.promo_type = 'CASHBACK' THEN\n" +
            "\t\t\tTZ.\"HARGACETAK_MEMBER/AGEN\" + CAST (X.promo_amount AS INTEGER)\n" +
            "\t\tWHEN X.promo_type = 'DISCOUNT' THEN\n" +
            "\t\t\tROUND(\n" +
            "\t\t\t\t(\n" +
            "\t\t\t\t\tTZ.\"HARGACETAK_MEMBER/AGEN\" * 10\n" +
            "\t\t\t\t) / (\n" +
            "\t\t\t\t\t10 - (\n" +
            "\t\t\t\t\t\tCAST (X.promo_amount AS INTEGER) * 0.1\n" +
            "\t\t\t\t\t)\n" +
            "\t\t\t\t)\n" +
            "\t\t\t)\n" +
            "\t\tELSE\n" +
            "\t\t\tTZ.\"HARGACETAK_MEMBER/AGEN\"\n" +
            "\t\tEND\n" +
            "\t\tELSE\n" +
            "\t\t\tTZ.\"HARGACETAK_MEMBER/AGEN\"\n" +
            "\t\tEND\n" +
            "\t\tFROM\n" +
            "\t\t\t\"MsPromo\" X\n" +
            "\t\tWHERE\n" +
            "\t\t\tX.id_promo = TZ.id_promo\n" +
            "\t)\n" +
            "ELSE\n" +
            "\tTZ.\"HARGACETAK_MEMBER/AGEN\"\n" +
            "END\n" +
            "ELSE\n" +
            "\tTZ.\"HARGACETAK_MEMBER/AGEN\"\n" +
            "END\n" +
            "WHEN TZ.\"STOCK_TYPE\" = 'MEMBER' THEN\n" +
            "\tCASE\n" +
            "WHEN TZ.\"flagActivePromoMember\" = 't' THEN\n" +
            "\tCASE\n" +
            "WHEN TZ.\"id_promoMember\" IS NOT NULL THEN\n" +
            "\t(\n" +
            "\t\tSELECT\n" +
            "\t\t\tCASE\n" +
            "\t\tWHEN TZ.\"timestamp\" :: DATE BETWEEN X.tgl_mulai\n" +
            "\t\tAND X.tgl_selesai THEN\n" +
            "\t\t\tCASE\n" +
            "\t\tWHEN X.promo_type = 'CASHBACK' THEN\n" +
            "\t\t\tTZ.\"HARGACETAK_MEMBER/AGEN\" + CAST (X.promo_amount AS INTEGER)\n" +
            "\t\tWHEN X.promo_type = 'DISCOUNT' THEN\n" +
            "\t\t\tROUND(\n" +
            "\t\t\t\t(\n" +
            "\t\t\t\t\tTZ.\"HARGACETAK_MEMBER/AGEN\" * 10\n" +
            "\t\t\t\t) / (\n" +
            "\t\t\t\t\t10 - (\n" +
            "\t\t\t\t\t\tCAST (X.promo_amount AS INTEGER) * 0.1\n" +
            "\t\t\t\t\t)\n" +
            "\t\t\t\t)\n" +
            "\t\t\t)\n" +
            "\t\tELSE\n" +
            "\t\t\tTZ.\"HARGACETAK_MEMBER/AGEN\"\n" +
            "\t\tEND\n" +
            "\t\tELSE\n" +
            "\t\t\tTZ.\"HARGACETAK_MEMBER/AGEN\"\n" +
            "\t\tEND\n" +
            "\t\tFROM\n" +
            "\t\t\t\"MsPromo\" X\n" +
            "\t\tWHERE\n" +
            "\t\t\tX.id_promo = TZ.\"id_promoMember\"\n" +
            "\t)\n" +
            "ELSE\n" +
            "\tTZ.\"HARGACETAK_MEMBER/AGEN\"\n" +
            "END\n" +
            "ELSE\n" +
            "\tTZ.\"HARGACETAK_MEMBER/AGEN\"\n" +
            "END\n" +
            "WHEN TZ.\"STOCK_TYPE\" = 'MTA' THEN\n" +
            "\tTZ.\"HARGACETAK_MEMBER/AGEN\"\n" +
            "END\n" +
            "ELSE\n" +
            "\tTZ.\"HARGACETAK_MEMBER/AGEN\"\n" +
            "END\n" +
            "ELSE\n" +
            "\tTZ.\"HARGACETAK_MEMBER/AGEN\"\n" +
            "END AS \"HARGACETAK_MEMBER/AGEN\",\n" +
            " (\n" +
            "\tTZ.\"HARGACETAK_MEMBER/AGEN\" - TZ.\"HARGAJUAL\"\n" +
            ") AS \"REVENUE_FROM_TRUE_TO_MEMBER/AGENT\",\n" +
            " (\n" +
            "\tTZ.\"HARGAJUAL\" - TZ.\"HARGABELI\"\n" +
            ") AS \"REVENUE_FROM_SUPPLIER_TO_TRUE\",\n" +
            " (\n" +
            "\tTZ.\"HARGACETAK_MEMBER/AGEN\" - TZ.\"HARGAJUAL\"\n" +
            ") + (\n" +
            "\tTZ.\"HARGAJUAL\" - TZ.\"HARGABELI\"\n" +
            ") AS \"GROSS_REVENUE\",\n" +
            " TZ.\"MIDA\" AS \"KOMISIAGEN\",\n" +
            " TZ.\"DD\" AS \"KOMISIDEALER\",\n" +
            " (\n" +
            "\t(\n" +
            "\t\tTZ.\"HARGACETAK_MEMBER/AGEN\" - TZ.\"HARGAJUAL\"\n" +
            "\t) + (\n" +
            "\t\tTZ.\"HARGAJUAL\" - TZ.\"HARGABELI\"\n" +
            "\t) - TZ.\"MIDA\" - TZ.\"DD\"\n" +
            ") AS \"NETREVENUE\",\n" +
            " CASE\n" +
            "WHEN TZ.\"NamaTipeTransaksi\" = 'Pulsa Prabayar' THEN\n" +
            "\tCASE\n" +
            "WHEN TZ.\"HARGAJUAL\" > TZ.\"HARGABELI\" THEN\n" +
            "\t(\n" +
            "\t\tSELECT\n" +
            "\t\t\tpromo_amount :: INT\n" +
            "\t\tFROM\n" +
            "\t\t\t\"tr_promo_log\"\n" +
            "\t\tWHERE\n" +
            "\t\t\tid_transaksi :: INT = TZ.id_transaksi\n" +
            "\t)\n" +
            "ELSE\n" +
            "\tTZ.\"MIDM\"\n" +
            "END\n" +
            "ELSE\n" +
            "\t(\n" +
            "\t\tSELECT\n" +
            "\t\t\tpromo_amount :: INT\n" +
            "\t\tFROM\n" +
            "\t\t\t\"tr_promo_log\"\n" +
            "\t\tWHERE\n" +
            "\t\t\tid_transaksi :: INT = TZ.id_transaksi\n" +
            "\t)\n" +
            "END AS \"CASHBACK\",\n" +
            " (\n" +
            "\t(\n" +
            "\t\tTZ.\"HARGACETAK_MEMBER/AGEN\" - TZ.\"HARGAJUAL\"\n" +
            "\t) + (\n" +
            "\t\tTZ.\"HARGAJUAL\" - TZ.\"HARGABELI\"\n" +
            "\t) - TZ.\"MIDA\" - TZ.\"MIDM\" - TZ.\"DD\"\n" +
            ") AS \"NETPROVIT\",\n" +
            " TZ.\"REMARKS_HARGA_JUAL\",\n" +
            " TZ.\"REMARKS_HARGA_CETAK\",\n" +
            " TZ.id_cabang,\n" +
            " TZ.\"NamaTipeAplikasi\",\n" +
            " TZ.\"NamaOperator\",\n" +
            " TZ.\"NoHPTujuan\",\n" +
            " TZ.\"HargaCetakMember\",\n" +
            " TZ.\"TrueFeeType\",\n" +
            " TZ.\"flagActivePromo\",\n" +
            " TZ.\"flagActivePromoMember\",\n" +
            " TZ.id_promo,\n" +
            " TZ.\"id_promoMember\",\n" +
            " CASE\n" +
            "WHEN TZ.\"NamaTipeTransaksi\" = 'Pulsa Prabayar' THEN\n" +
            "\tCASE\n" +
            "WHEN TZ.\"HARGAJUAL\" < TZ.\"HARGABELI\" THEN\n" +
            "\tCASE\n" +
            "WHEN TZ.\"STOCK_TYPE\" = 'AGENT' THEN\n" +
            "\tCASE\n" +
            "WHEN TZ.\"flagActivePromo\" = 't' THEN\n" +
            "\tCASE\n" +
            "WHEN TZ.\"id_promo\" IS NOT NULL THEN\n" +
            "\t(\n" +
            "\t\tSELECT\n" +
            "\t\t\tCASE\n" +
            "\t\tWHEN TZ.\"timestamp\" :: DATE BETWEEN X.tgl_mulai\n" +
            "\t\tAND X.tgl_selesai THEN\n" +
            "\t\t\tTZ.\"HARGAJUAL\"\n" +
            "\t\tELSE\n" +
            "\t\t\t0\n" +
            "\t\tEND\n" +
            "\t\tFROM\n" +
            "\t\t\t\"MsPromo\" X\n" +
            "\t\tWHERE\n" +
            "\t\t\tX.id_promo = TZ.\"id_promo\"\n" +
            "\t)\n" +
            "ELSE\n" +
            "\t0\n" +
            "END\n" +
            "ELSE\n" +
            "\t0\n" +
            "END\n" +
            "WHEN TZ.\"STOCK_TYPE\" = 'MEMBER' THEN\n" +
            "\tCASE\n" +
            "WHEN TZ.\"flagActivePromoMember\" = 't' THEN\n" +
            "\tCASE\n" +
            "WHEN TZ.\"id_promoMember\" IS NOT NULL THEN\n" +
            "\t(\n" +
            "\t\tSELECT\n" +
            "\t\t\tCASE\n" +
            "\t\tWHEN TZ.\"timestamp\" :: DATE BETWEEN X.tgl_mulai\n" +
            "\t\tAND X.tgl_selesai THEN\n" +
            "\t\t\tTZ.\"HARGAJUAL\"\n" +
            "\t\tELSE\n" +
            "\t\t\t0\n" +
            "\t\tEND\n" +
            "\t\tFROM\n" +
            "\t\t\t\"MsPromo\" X\n" +
            "\t\tWHERE\n" +
            "\t\t\tX.id_promo = TZ.\"id_promoMember\"\n" +
            "\t)\n" +
            "ELSE\n" +
            "\t0\n" +
            "END\n" +
            "ELSE\n" +
            "\t0\n" +
            "END\n" +
            "ELSE\n" +
            "\t0\n" +
            "END\n" +
            "ELSE\n" +
            "\t0\n" +
            "END\n" +
            "ELSE\n" +
            "\tCASE\n" +
            "WHEN promo_type = 'DISCOUNT' THEN\n" +
            "\tTZ.\"HARGACETAK_MEMBER/AGEN\" - B.promo_amount :: INT\n" +
            "ELSE\n" +
            "\t0\n" +
            "END\n" +
            "END AS \"HARGAJUALPROMO\",\n" +
            " CASE\n" +
            "WHEN TZ.\"NamaTipeTransaksi\" = 'Pulsa Prabayar' THEN\n" +
            "\tCASE\n" +
            "WHEN TZ.\"STOCK_TYPE\" = 'AGENT' THEN\n" +
            "\tCASE\n" +
            "WHEN TZ.\"flagActivePromo\" = 't' THEN\n" +
            "\t(\n" +
            "\t\tSELECT\n" +
            "\t\t\tCAST (X.promo_amount AS INTEGER)\n" +
            "\t\tFROM\n" +
            "\t\t\t\"MsPromo\" X\n" +
            "\t\tWHERE\n" +
            "\t\t\tX.id_promo = TZ.id_promo\n" +
            "\t)\n" +
            "ELSE\n" +
            "\t0\n" +
            "END\n" +
            "WHEN TZ.\"STOCK_TYPE\" = 'MEMBER' THEN\n" +
            "\tCASE\n" +
            "WHEN TZ.\"flagActivePromoMember\" = 't' THEN\n" +
            "\t(\n" +
            "\t\tSELECT\n" +
            "\t\t\tCAST (X.promo_amount AS INTEGER)\n" +
            "\t\tFROM\n" +
            "\t\t\t\"MsPromo\" X\n" +
            "\t\tWHERE\n" +
            "\t\t\tX.id_promo = TZ.\"id_promoMember\"\n" +
            "\t)\n" +
            "ELSE\n" +
            "\t0\n" +
            "END\n" +
            "ELSE\n" +
            "\t0\n" +
            "END\n" +
            "ELSE\n" +
            "\tCASE\n" +
            "WHEN B.promo_type = 'DISCOUNT' THEN\n" +
            "\tB.promo_amount :: INT\n" +
            "ELSE\n" +
            "\t0\n" +
            "END\n" +
            "END AS \"PROMO\",\n" +
            " CASE\n" +
            "WHEN TZ.\"NamaTipeTransaksi\" = 'Pulsa Prabayar' THEN\n" +
            "\tCASE\n" +
            "WHEN TZ.\"STOCK_TYPE\" = 'AGENT' THEN\n" +
            "\t(\n" +
            "\t\tSELECT\n" +
            "\t\t\tX.promo_type\n" +
            "\t\tFROM\n" +
            "\t\t\t\"MsPromo\" X\n" +
            "\t\tWHERE\n" +
            "\t\t\tX.id_promo = TZ.id_promo\n" +
            "\t)\n" +
            "WHEN TZ.\"STOCK_TYPE\" = 'MEMBER' THEN\n" +
            "\t(\n" +
            "\t\tSELECT\n" +
            "\t\t\tX.promo_type\n" +
            "\t\tFROM\n" +
            "\t\t\t\"MsPromo\" X\n" +
            "\t\tWHERE\n" +
            "\t\t\tX.id_promo = TZ.\"id_promoMember\"\n" +
            "\t)\n" +
            "END\n" +
            "ELSE\n" +
            "\tB.promo_type\n" +
            "END AS \"PROMOTYPE\",\n" +
            " TZ.\"id_pelanggan\"\n" +
            "FROM\n" +
            "\tTZ\n" +
            "LEFT JOIN tr_promo_log B ON TZ.id_transaksi = B.id_transaksi :: INT\n" +
            "),\n" +
            " TZ2 AS (\n" +
            "\tSELECT DISTINCT\n" +
            "\t\tTZ1.id_transaksi AS \"ID\",\n" +
            "\t\tTZ1.\"IDTRANSAKSI\",\n" +
            "\t\tTZ1.\"IDTRANSAKSI_SUPPLIER\",\n" +
            "\t\tTZ1.\"TANGGAL\",\n" +
            "\t\tTZ1.\"WAKTU\",\n" +
            "\t\tTZ1.id_agent AS \"ID_AGENT\",\n" +
            "\t\tTZ1.\"id_agentaccount\" AS \"ID_AGENTACCOUNT\",\n" +
            "\t\tTZ1.\"NomorKartu\" AS \"ID_AGENTCARD\",\n" +
            "\t\tTZ1.\"Nama\" AS \"NAMA_AGEN\",\n" +
            "\t\tA .\"LokasiCabang\" AS \"LOKASI\",\n" +
            "\t\tTZ1.id_memberaccount AS \"ID_MEMBER\",\n" +
            "\t\tTZ1.\"STOCK_TYPE\",\n" +
            "\t\tTZ1.\"NamaTipeAplikasi\" AS \"CHANNEL\",\n" +
            "\t\tTZ1.\"NamaTipeTransaksi\" AS \"SUB_TIPE_TRANSAKSI\",\n" +
            "\t\tTZ1.\"CustomerReference\" AS \"SN\",\n" +
            "\t\tTZ1.\"StatusTRX\" AS \"STATUS_TRANSAKSI\",\n" +
            "\t\tTZ1.\"Keterangan\" AS \"KETERANGAN\",\n" +
            "\t\t'TOP UP' AS \"TIPE_TRANSAKSI\",\n" +
            "\t\tCASE\n" +
            "\tWHEN TZ1.\"NamaTipeTransaksi\" = 'Pembelian Tiket Kereta' THEN\n" +
            "\t\t(\n" +
            "\t\t\tSELECT\n" +
            "\t\t\t\t\"Nominal\"\n" +
            "\t\t\tFROM\n" +
            "\t\t\t\t\"TrTransaksi\"\n" +
            "\t\t\tWHERE\n" +
            "\t\t\t\tid_transaksi = TZ1.id_transaksi\n" +
            "\t\t) :: INT\n" +
            "\tELSE\n" +
            "\t\tTZ1.\"Nominal\" :: INT\n" +
            "\tEND AS \"NOMINAL\",\n" +
            "\tCASE\n" +
            "WHEN UPPER (TZ1.\"NamaSupplier\") = 'GEMILANG'\n" +
            "AND TZ1.\"TANGGAL\" :: DATE >= '2017-05-18' THEN\n" +
            "\t'Gemilang Stock'\n" +
            "ELSE\n" +
            "\tTZ1.\"NamaSupplier\"\n" +
            "END AS \"NAMA_SUPPLIER\",\n" +
            " TZ1.\"NamaOperator\" AS \"NAMA_OPERATOR\",\n" +
            " CASE\n" +
            "WHEN TZ1.\"NamaTipeTransaksi\" = 'Pembelian Tiket Kereta' THEN\n" +
            "\t(\n" +
            "\t\tSELECT\n" +
            "\t\t\thargabeli\n" +
            "\t\tFROM\n" +
            "\t\t\t\"TrTransaksi\"\n" +
            "\t\tWHERE\n" +
            "\t\t\tid_transaksi = TZ1.id_transaksi\n" +
            "\t) :: INT\n" +
            "ELSE\n" +
            "\tTZ1.\"HARGABELI\" :: INT\n" +
            "END AS \"HARGABELI\",\n" +
            " CASE\n" +
            "WHEN TZ1.\"NamaTipeTransaksi\" = 'Pembelian Tiket Kereta' THEN\n" +
            "\t(\n" +
            "\t\tSELECT\n" +
            "\t\t\thargajual\n" +
            "\t\tFROM\n" +
            "\t\t\t\"TrTransaksi\"\n" +
            "\t\tWHERE\n" +
            "\t\t\tid_transaksi = TZ1.id_transaksi\n" +
            "\t)\n" +
            "ELSE\n" +
            "\tTZ1.\"HARGAJUAL\" :: INT\n" +
            "END AS \"HARGAJUAL\",\n" +
            " CASE\n" +
            "WHEN TZ1.\"NamaTipeTransaksi\" = 'Pembelian Tiket Kereta' THEN\n" +
            "\t(\n" +
            "\t\tSELECT\n" +
            "\t\t\thargajual\n" +
            "\t\tFROM\n" +
            "\t\t\t\"TrTransaksi\"\n" +
            "\t\tWHERE\n" +
            "\t\t\tid_transaksi = TZ1.id_transaksi\n" +
            "\t)\n" +
            "ELSE\n" +
            "\tTZ1.\"HARGACETAK_MEMBER/AGEN\"\n" +
            "END AS \"HARGACETAK_MEMBER/AGEN\",\n" +
            " CASE\n" +
            "WHEN TZ1.\"NamaTipeTransaksi\" = 'Pembelian Tiket Kereta' THEN\n" +
            "\t(\n" +
            "\t\tSELECT\n" +
            "\t\t\t\"Nominal\"\n" +
            "\t\tFROM\n" +
            "\t\t\t\"TrTransaksi\"\n" +
            "\t\tWHERE\n" +
            "\t\t\tid_transaksi = TZ1.id_transaksi\n" +
            "\t) :: INT - (\n" +
            "\t\tSELECT\n" +
            "\t\t\t\"Total\"\n" +
            "\t\tFROM\n" +
            "\t\t\t\"TrTransaksi\"\n" +
            "\t\tWHERE\n" +
            "\t\t\tid_transaksi = TZ1.id_transaksi\n" +
            "\t) :: INT\n" +
            "ELSE\n" +
            "\tTZ1.\"REVENUE_FROM_TRUE_TO_MEMBER/AGENT\"\n" +
            "END AS \"REVENUE_FROM_TRUE_TO_MEMBER/AGENT\",\n" +
            " CASE\n" +
            "WHEN TZ1.\"NamaTipeTransaksi\" = 'Pembelian Tiket Kereta' THEN\n" +
            "\t(\n" +
            "\t\tSELECT\n" +
            "\t\t\t\"Biaya\"\n" +
            "\t\tFROM\n" +
            "\t\t\t\"TrTransaksi\"\n" +
            "\t\tWHERE\n" +
            "\t\t\tid_transaksi = TZ1.id_transaksi\n" +
            "\t) :: INT\n" +
            "ELSE\n" +
            "\tTZ1.\"REVENUE_FROM_SUPPLIER_TO_TRUE\"\n" +
            "END AS \"REVENUE_FROM_SUPPLIER_TO_TRUE\",\n" +
            " CASE\n" +
            "WHEN TZ1.\"NamaTipeTransaksi\" = 'Pembelian Tiket Kereta' THEN\n" +
            "\t(\n" +
            "\t\tSELECT\n" +
            "\t\t\thargajual\n" +
            "\t\tFROM\n" +
            "\t\t\t\"TrTransaksi\"\n" +
            "\t\tWHERE\n" +
            "\t\t\tid_transaksi = TZ1.id_transaksi\n" +
            "\t) - (\n" +
            "\t\tSELECT\n" +
            "\t\t\thargabeli\n" +
            "\t\tFROM\n" +
            "\t\t\t\"TrTransaksi\"\n" +
            "\t\tWHERE\n" +
            "\t\t\tid_transaksi = TZ1.id_transaksi\n" +
            "\t) :: INT\n" +
            "ELSE\n" +
            "\tTZ1.\"GROSS_REVENUE\"\n" +
            "END AS \"GROSS_REVENUE\",\n" +
            " CASE\n" +
            "WHEN TZ1.\"NamaTipeTransaksi\" = 'Pembelian Tiket Kereta' THEN\n" +
            "\t0\n" +
            "WHEN TZ1.\"NamaTipeTransaksi\" = 'Pulsa Prabayar' THEN\n" +
            "\t0\n" +
            "WHEN TZ1.\"NamaTipeTransaksi\" = 'Topup Gopay' THEN\n" +
            "\tCASE\n" +
            "WHEN (\n" +
            "\tSELECT\n" +
            "\t\t\"Nominal\"\n" +
            "\tFROM\n" +
            "\t\t\"TrStock\"\n" +
            "\tWHERE\n" +
            "\t\tid_transaksi = TZ1.id_transaksi\n" +
            "\tAND UPPER (\"StokType\") = 'AGENT'\n" +
            "\tAND UPPER (\"TypeTrx\") = 'DEPOSIT'\n" +
            ") IS NULL THEN\n" +
            "\t0\n" +
            "ELSE\n" +
            "\t(\n" +
            "\t\tSELECT\n" +
            "\t\t\t\"Nominal\"\n" +
            "\t\tFROM\n" +
            "\t\t\t\"TrStock\"\n" +
            "\t\tWHERE\n" +
            "\t\t\tid_transaksi = TZ1.id_transaksi\n" +
            "\t\tAND UPPER (\"StokType\") = 'AGENT'\n" +
            "\t\tAND UPPER (\"TypeTrx\") = 'DEPOSIT'\n" +
            "\t)\n" +
            "END\n" +
            "ELSE\n" +
            "\tTZ1.\"KOMISIAGEN\"\n" +
            "END AS \"KOMISIAGEN\",\n" +
            " TZ1.\"KOMISIDEALER\",\n" +
            " CASE\n" +
            "WHEN TZ1.\"NamaTipeTransaksi\" = 'Pembelian Tiket Kereta' THEN\n" +
            "\t(\n" +
            "\t\tSELECT\n" +
            "\t\t\thargajual\n" +
            "\t\tFROM\n" +
            "\t\t\t\"TrTransaksi\"\n" +
            "\t\tWHERE\n" +
            "\t\t\tid_transaksi = TZ1.id_transaksi\n" +
            "\t) - (\n" +
            "\t\tSELECT\n" +
            "\t\t\thargabeli\n" +
            "\t\tFROM\n" +
            "\t\t\t\"TrTransaksi\"\n" +
            "\t\tWHERE\n" +
            "\t\t\tid_transaksi = TZ1.id_transaksi\n" +
            "\t)\n" +
            "ELSE\n" +
            "\tTZ1.\"NETREVENUE\"\n" +
            "END AS \"NETREVENUE\",\n" +
            " CASE\n" +
            "WHEN TZ1.\"CASHBACK\" IS NULL THEN\n" +
            "\t'0'\n" +
            "ELSE\n" +
            "\tTZ1.\"CASHBACK\"\n" +
            "END AS \"CASHBACK\",\n" +
            " CASE\n" +
            "WHEN TZ1.\"NamaTipeTransaksi\" = 'Pembelian Tiket Kereta' THEN\n" +
            "\t(\n" +
            "\t\tSELECT\n" +
            "\t\t\thargajual\n" +
            "\t\tFROM\n" +
            "\t\t\t\"TrTransaksi\"\n" +
            "\t\tWHERE\n" +
            "\t\t\tid_transaksi = TZ1.id_transaksi\n" +
            "\t) - (\n" +
            "\t\tSELECT\n" +
            "\t\t\thargabeli\n" +
            "\t\tFROM\n" +
            "\t\t\t\"TrTransaksi\"\n" +
            "\t\tWHERE\n" +
            "\t\t\tid_transaksi = TZ1.id_transaksi\n" +
            "\t)\n" +
            "ELSE\n" +
            "\tTZ1.\"NETPROVIT\"\n" +
            "END AS \"NETPROVIT\",\n" +
            " CASE\n" +
            "WHEN TZ1.timestampupdatetrx IS NULL THEN\n" +
            "\tTZ1.\"timestamp\"\n" +
            "ELSE\n" +
            "\tTZ1.timestampupdatetrx\n" +
            "END AS \"timestampupdate\",\n" +
            " TZ1.timestamprefund,\n" +
            " CASE\n" +
            "WHEN TZ1.\"HARGABELI\" = '0' THEN\n" +
            "\t0\n" +
            "WHEN TZ1.\"HARGAJUAL\" = '0' THEN\n" +
            "\t0\n" +
            "WHEN TZ1.\"HARGACETAK_MEMBER/AGEN\" = '0' THEN\n" +
            "\t0\n" +
            "WHEN TZ1.\"StatusTRX\" <> 'SUKSES' THEN\n" +
            "\t0\n" +
            "ELSE\n" +
            "\t1\n" +
            "END AS \"FLAG_CLEAN\",\n" +
            " NULL AS \"FLAG_FINANCE\",\n" +
            " TZ1.\"NoHPTujuan\",\n" +
            " CASE\n" +
            "WHEN TZ1.\"NamaTipeTransaksi\" = 'Pembelian Tiket Kereta' THEN\n" +
            "\t(\n" +
            "\t\tSELECT\n" +
            "\t\t\thargajual\n" +
            "\t\tFROM\n" +
            "\t\t\t\"TrTransaksi\"\n" +
            "\t\tWHERE\n" +
            "\t\t\tid_transaksi = TZ1.id_transaksi\n" +
            "\t)\n" +
            "ELSE\n" +
            "\tTZ1.\"HargaCetakMember\"\n" +
            "END AS \"HARGA_CETAK_FP\",\n" +
            " CASE\n" +
            "WHEN TZ1.\"NamaTipeTransaksi\" = 'Pembelian Tiket Kereta' THEN\n" +
            "\t0\n" +
            "ELSE\n" +
            "\t(\n" +
            "\t\tTZ1.\"HargaCetakMember\" - TZ1.\"HARGAJUAL\"\n" +
            "\t)\n" +
            "END AS \"POTONGAN_HARGA_FP\",\n" +
            " CASE\n" +
            "WHEN TZ1.\"NamaTipeTransaksi\" = 'Pembelian Tiket Kereta' THEN\n" +
            "\t(\n" +
            "\t\tSELECT\n" +
            "\t\t\thargajual\n" +
            "\t\tFROM\n" +
            "\t\t\t\"TrTransaksi\"\n" +
            "\t\tWHERE\n" +
            "\t\t\tid_transaksi = TZ1.id_transaksi\n" +
            "\t) - (\n" +
            "\t\tSELECT\n" +
            "\t\t\thargabeli\n" +
            "\t\tFROM\n" +
            "\t\t\t\"TrTransaksi\"\n" +
            "\t\tWHERE\n" +
            "\t\t\tid_transaksi = TZ1.id_transaksi\n" +
            "\t)\n" +
            "ELSE\n" +
            "\t(\n" +
            "\t\tTZ1.\"HargaCetakMember\" - TZ1.\"HARGABELI\"\n" +
            "\t)\n" +
            "END AS \"HARGA_CETAK-HARGA_BELI_FP\",\n" +
            " CASE\n" +
            "WHEN TZ1.\"NamaTipeTransaksi\" = 'Pembelian Tiket Kereta' THEN\n" +
            "\t(\n" +
            "\t\tSELECT\n" +
            "\t\t\thargajual\n" +
            "\t\tFROM\n" +
            "\t\t\t\"TrTransaksi\"\n" +
            "\t\tWHERE\n" +
            "\t\t\tid_transaksi = TZ1.id_transaksi\n" +
            "\t) - (\n" +
            "\t\tSELECT\n" +
            "\t\t\thargabeli\n" +
            "\t\tFROM\n" +
            "\t\t\t\"TrTransaksi\"\n" +
            "\t\tWHERE\n" +
            "\t\t\tid_transaksi = TZ1.id_transaksi\n" +
            "\t) :: INT\n" +
            "ELSE\n" +
            "\t(\n" +
            "\t\t(\n" +
            "\t\t\tTZ1.\"HargaCetakMember\" - TZ1.\"HARGABELI\"\n" +
            "\t\t) - (\n" +
            "\t\t\tTZ1.\"HargaCetakMember\" - TZ1.\"HARGAJUAL\"\n" +
            "\t\t)\n" +
            "\t)\n" +
            "END AS \"GROSS_REVENUE_FP\",\n" +
            " TZ1.\"TrueFeeType\",\n" +
            " TZ1.\"HARGAJUALPROMO\",\n" +
            " TZ1.\"PROMO\",\n" +
            " TZ1.\"PROMOTYPE\",\n" +
            " CASE\n" +
            "WHEN TZ1.\"NamaTipeTransaksi\" = 'Pulsa Prabayar' THEN\n" +
            "\tCASE\n" +
            "WHEN TZ1.\"STOCK_TYPE\" = 'AGENT' THEN\n" +
            "\tCASE\n" +
            "WHEN TZ1.\"flagActivePromo\" = 't' THEN\n" +
            "\t(\n" +
            "\t\tSELECT\n" +
            "\t\t\tCASE\n" +
            "\t\tWHEN X.promo_type = 'CASHBACK' THEN\n" +
            "\t\t\tTZ1.\"HARGAJUAL\" - TZ1.\"HARGAJUALPROMO\"\n" +
            "\t\tWHEN X.promo_type = 'DISCOUNT' THEN\n" +
            "\t\t\t(TZ1.\"HARGAJUAL\") * (\n" +
            "\t\t\t\tCAST (X.promo_amount AS INTEGER) * 0.01\n" +
            "\t\t\t)\n" +
            "\t\tELSE\n" +
            "\t\t\t0\n" +
            "\t\tEND\n" +
            "\t\tFROM\n" +
            "\t\t\t\"MsPromo\" X\n" +
            "\t\tWHERE\n" +
            "\t\t\tX.id_promo = TZ1.id_promo\n" +
            "\t)\n" +
            "ELSE\n" +
            "\t0\n" +
            "END\n" +
            "WHEN TZ1.\"STOCK_TYPE\" = 'MEMBER' THEN\n" +
            "\tCASE\n" +
            "WHEN TZ1.\"flagActivePromo\" = 't' THEN\n" +
            "\t(\n" +
            "\t\tSELECT\n" +
            "\t\t\tCASE\n" +
            "\t\tWHEN X.promo_type = 'CASHBACK' THEN\n" +
            "\t\t\tTZ1.\"HARGAJUAL\" - TZ1.\"HARGAJUALPROMO\"\n" +
            "\t\tWHEN X.promo_type = 'DISCOUNT' THEN\n" +
            "\t\t\t(TZ1.\"HARGAJUAL\") * (\n" +
            "\t\t\t\tCAST (X.promo_amount AS INTEGER) * 0.01\n" +
            "\t\t\t)\n" +
            "\t\tELSE\n" +
            "\t\t\t0\n" +
            "\t\tEND\n" +
            "\t\tFROM\n" +
            "\t\t\t\"MsPromo\" X\n" +
            "\t\tWHERE\n" +
            "\t\t\tX.id_promo = TZ1.\"id_promoMember\"\n" +
            "\t)\n" +
            "ELSE\n" +
            "\t0\n" +
            "END\n" +
            "ELSE\n" +
            "\t0\n" +
            "END\n" +
            "ELSE\n" +
            "\tCASE\n" +
            "WHEN E.promo_type = 'DISCOUNT' THEN\n" +
            "\tE.promo_amount :: INT\n" +
            "ELSE\n" +
            "\t0\n" +
            "END\n" +
            "END AS \"JUMLAHDISKON\",\n" +
            " TZ1.\"id_pelanggan\" AS \"ID_PELANGGAN\",\n" +
            " C .applicant_full_name,\n" +
            " C .applicant_national_id,\n" +
            " D.\"transactionIdGateway\"\n" +
            "FROM\n" +
            "\tTZ1\n" +
            "LEFT OUTER JOIN \"MsCabang\" A ON TZ1.id_cabang = A .id_cabang\n" +
            "LEFT JOIN \"tr_customer_data\" C ON TZ1.\"IDTRANSAKSI\" = C .id_trx\n" +
            "LEFT JOIN \"TrPulsaGemilang\" D ON TZ1.\"IDTRANSAKSI\" = D.trxidpartner\n" +
            "LEFT JOIN \"tr_promo_log\" E ON TZ1.\"IDTRANSAKSI\" = E.id_transaksi\n" +
            ") SELECT\n" +
            "\t*\n" +
            "FROM\n" +
            "\tTZ2\n" +
            "ORDER BY\n" +
            "\tTZ2.\"ID\";";
}
