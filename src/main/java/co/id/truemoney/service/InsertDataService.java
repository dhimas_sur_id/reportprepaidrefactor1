package co.id.truemoney.service;

import co.id.truemoney.Dto.TmnPrepaidDto;
import co.id.truemoney.config.ConnectionConfig;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.ExecutorService;

import org.apache.log4j.Logger;
import org.javalite.activejdbc.Base;

public class InsertDataService extends ConnectionConfig {

    final static Logger LOG = Logger.getLogger(InsertDataService.class);

    public void insertAll(List<TmnPrepaidDto> data) throws SQLException {
        try {
            ExecutorService executor = java.util.concurrent.Executors.newFixedThreadPool(100);
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    openLokal();
                    PreparedStatement ps = Base.startBatch(sql);
                    for (int k = 0; k < data.size(); k++) {
                        Base.addBatch(ps,
                                data.get(k).getId(),
                                data.get(k).getIdtransaksi(),
                                data.get(k).getIdtransaksiSupplier(),
                                data.get(k).getTanggal(),
                                data.get(k).getWaktu(),
                                data.get(k).getIdAgent(),
                                data.get(k).getIdAgentAccount(),
                                data.get(k).getIdAgentCard(),
                                data.get(k).getNamaAgent(),
                                data.get(k).getLokasi(),
                                data.get(k).getIdMember(),
                                data.get(k).getStockType(),
                                data.get(k).getChannel(),
                                data.get(k).getSubTipeTransaksi(),
                                data.get(k).getSn(),
                                data.get(k).getStatusTraksaksi(),
                                data.get(k).getKeterangan(),
                                data.get(k).getTipeTransaksi(),
                                data.get(k).getNominal(),
                                data.get(k).getNamaSupplier(),
                                data.get(k).getNamaOperator(),
                                data.get(k).getHargaBeli(),
                                data.get(k).getHargaJual(),
                                data.get(k).getHargaCetakMemberOrAgent(),
                                data.get(k).getRevenueFromTrueToMemberOrAgent(),
                                data.get(k).getRevenueFromSupplierToTrue(),
                                data.get(k).getGrossRevenue(),
                                data.get(k).getKomisiAgen(),
                                data.get(k).getKomisiDealer(),
                                data.get(k).getNetRevenue(),
                                data.get(k).getCashBack(),
                                data.get(k).getNetProvit(),
                                data.get(k).getTimestampupdate(),
                                data.get(k).getTimestamprefund(),
                                data.get(k).getFlagClean(),
                                data.get(k).getFlagFinnacne(),
                                data.get(k).getNoHpTujuan(),
                                data.get(k).getHargaCetakFp(),
                                data.get(k).getPotonganHargaFp(),
                                data.get(k).getHargaCetakKurangHargaBeliFp(),
                                data.get(k).getGrossRevenueFp(),
                                data.get(k).getTipeKomisi(),
                                data.get(k).getTotal(),
                                data.get(k).getHargaCetakPromo(),
                                data.get(k).getPromo(),
                                data.get(k).getPromoType(),
                                data.get(k).getBonus(),
                                data.get(k).getIdPelanggan(),
                                data.get(k).getNamaApplicant(),
                                data.get(k).getApplicantNationalId(),
                                data.get(k).getTransactionIdGateway(),
                                data.get(k).getImporteddate());
                        Base.executeBatch(ps);
                    }
                }
            });
            executor.shutdown();
            LOG.info("inserted row");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeLokal();
        }
    }

    final String sql = "insert\n"
            + "\tinto\n"
            + "\t\"TMN2_PREPAID\" (\"ID\",\n"
            + "\t\t\"IDTRANSAKSI\",\n"
            + "\t\t\"IDTRANSAKSI_SUPPLIER\",\n"
            + "\t\t\"TANGGAL\",\n"
            + "\t\t\"WAKTU\",\n"
            + "\t\t\"ID_AGENT\",\n"
            + "\t\t\"ID_AGENTACCOUNT\",\n"
            + "\t\t\"ID_AGENTCARD\",\n"
            + "\t\t\"NAMA_AGEN\",\n"
            + "\t\t\"LOKASI\",\n"
            + "\t\t\"ID_MEMBER\",\n"
            + "\t\t\"STOCK_TYPE\",\n"
            + "\t\t\"CHANNEL\",\n"
            + "\t\t\"SUB_TIPE_TRANSAKSI\",\n"
            + "\t\t\"SN\",\n"
            + "\t\t\"STATUS_TRANSAKSI\",\n"
            + "\t\t\"KETERANGAN\",\n"
            + "\t\t\"TIPE_TRANSAKSI\",\n"
            + "\t\t\"NOMINAL\",\n"
            + "\t\t\"NAMA_SUPPLIER\",\n"
            + "\t\t\"NAMA_OPERATOR\",\n"
            + "\t\t\"HARGABELI\",\n"
            + "\t\t\"HARGAJUAL\",\n"
            + "\t\t\"HARGACETAK_MEMBER/AGEN\",\n"
            + "\t\t\"REVENUE_FROM_TRUE_TO_MEMBER/AGENT\",\n"
            + "\t\t\"REVENUE_FROM_SUPPLIER_TO_TRUE\",\n"
            + "\t\t\"GROSS_REVENUE\",\n"
            + "\t\t\"KOMISIAGEN\",\n"
            + "\t\t\"KOMISIDEALER\",\n"
            + "\t\t\"NETREVENUE\",\n"
            + "\t\t\"CASHBACK\",\n"
            + "\t\t\"NETPROVIT\",\n"
            + "\t\ttimestampupdate,\n"
            + "\t\ttimestamprefund,\n"
            + "\t\t\"FLAG_CLEAN\",\n"
            + "\t\t\"FLAG_FINANCE\",\n"
            + "\t\t\"NoHpTujuan\",\n"
            + "\t\t\"HARGA_CETAK_FP\",\n"
            + "\t\t\"POTONGAN_HARGA_FP\",\n"
            + "\t\t\"HARGA_CETAK-HARGA_BELI_FP\",\n"
            + "\t\t\"GROSS_REVENUE_FP\",\n"
            + "\t\t\"TIPE_KOMISI\",\n"
            + "\t\t\"TOTAL\",\n"
            + "\t\t\"HARGACETAKPROMO\",\n"
            + "\t\t\"PROMO\",\n"
            + "\t\t\"PROMO_TYPE\",\n"
            + "\t\t\"BONUS\",\n"
            + "\t\t\"ID_PELANGGAN\",\n"
            + "\t\t\"NAMA_APPLICANT\",\n"
            + "\t\t\"APPLICANT_NATIONAL_ID\",\n"
            + "\t\t\"TRANSACTION_ID_GATEWAY\",\n"
            + "\t\tcreated_date)\n"
            + "\tvalues(\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t?,\n"
            + "\t? )\n ";

    public void deleteData(Date start, Date end) {
        try {
            openLokal();
            Base.exec(sqlDelete, start, end);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeLokal();
        }
    }

    //    private String sqlDelete = "delete from \"TMN2_PREPAID\" where created_date::DATE BETWEEN (?) and (?)";
    private String sqlDelete = "DELETE FROM \"TMN2_PREPAID\" \n"
            + "WHERE \"TANGGAL\"::DATE \n"
            + "BETWEEN (?) and (?)  AND \"SUB_TIPE_TRANSAKSI\" \n"
            + "IN ('Pulsa Prabayar','Pembelian Pulsa','PLN Prabayar','Asuransi',\n"
            + "'Pembelian Tiket Kereta','Top Up GoPay','Topup Gopay');";
}
