package co.id.truemoney.config;

import org.javalite.activejdbc.Base;

public abstract class ConnectionConfig {
    //Reporting Db
    private final String JDBC_URL = "jdbc:postgresql://172.16.10.123:5432/STAGING_REPORT_DEV?rewriteBatchedStatements=true";
    private final String JDBC_DRIVER = "org.postgresql.Driver";
    private final String USERNAME = "postgres";
    private final String PASSWORD = "truemoney2016";

    protected void openLokal() {
        Base.open(JDBC_DRIVER, JDBC_URL, USERNAME, PASSWORD);
    }

    protected void closeLokal() {
        Base.close();
    }

    // Production Db
    private final String JDBC_URL_1 = "jdbc:postgresql://172.16.80.101:5432/TMN";
    private final String JDBC_DRIVER_1 = "org.postgresql.Driver";
    private final String USERNAME_1 = "web_report";
    private final String PASSWORD_1 = "p13rt0tuml0c0m0t0r";

    protected void openFetch() {
        Base.open(JDBC_DRIVER_1, JDBC_URL_1, USERNAME_1, PASSWORD_1);
    }

    protected void closeFetch() {
        Base.close();
    }


}
